# -*- coding: utf-8 -*-

"""
cddis_downloader.py

Given a date (the_date),
download the attitude information (body quaternions & solar panel rotation angles)
for the "extended" week (9 days; Saturday to next Sunday).
"""

import datetime
import dateutil
import logging
import multiprocessing
import pathlib
import random
import string

import requests
from opnieuw import retry

from date_utils import infer_dates, check_year


BASE_URL = "https://cddis.nasa.gov/archive/doris/ancillary/quaternions"
SAVE_DIR = "./q"
SATELLITE = "ja3"  # ja1, ja2, ja3 or swo
DATA_TYPES = {"body": "qbody", "panels": "qsolp"}
LOGGING_LEVEL = logging.INFO
# LOGGING_LEVEL = logging.DEBUG  # try logging.DEBUG for more info

logging.basicConfig(
    format="%(module)s -- %(levelname)s: %(message)s", level=LOGGING_LEVEL
)


class CDDISDownloader:
    """Downloader object."""

    def __init__(self, base_url: str = BASE_URL, satellite: str = SATELLITE) -> None:
        """Default constructor."""
        self.base_url = base_url
        self.satellite = satellite

    @retry(
        retry_on_exceptions=(
            requests.exceptions.ConnectionError,
            requests.exceptions.HTTPError,
        ),
        max_calls_total=4,
        retry_window_after_first_call_in_seconds=60,
    )
    def _get_directory_listing(self, url: str) -> str:
        """List the files on the server directory."""
        # add the required suffix and make the request
        response = requests.get(f"{url}/*?list")
        response.raise_for_status()  # raise an exception for bad requests

        return response.text

    def _generate_urls(self, the_date: datetime.date, data_type: str) -> list[str]:
        """Generate the URL for the given date and data type."""
        # check if the "extended" week ends in the next year
        the_dates = check_year(infer_dates(the_date))

        # for each sublist of 'the_dates' generate the urls
        urls = []
        for date_list in the_dates:
            # construct the directory URL and request for the directory listing
            url = f"{self.base_url}/{self.satellite}/{str(date_list[0].year)}"
            listing = self._get_directory_listing(url).split("\n")[:-1]

            # find the matching files in the directory listing
            file_prefixes = [
                f"{self.satellite}{data_type}{d.strftime('%Y%m%d')}" for d in date_list
            ]
            logging.debug(file_prefixes)

            matches = [
                f for f in listing if any(f.startswith(p) for p in file_prefixes)
            ]
            logging.debug(matches)

            # construct urls to download
            for m in matches:
                urls.append(f"{url}/{m.split()[0]}")

        logging.debug(urls)
        return urls

    def _make_save_dir(self, save_dir: str) -> None:
        """
        Make the directory to save the files;
        if a file with the same name exists, add a random suffix to the given name.
        """
        try:
            pathlib.Path(save_dir).mkdir(parents=True, exist_ok=True)
        except FileExistsError:
            sfx = "".join(random.choices(string.ascii_uppercase + string.digits, k=6))
            pathlib.Path(f"{save_dir}_{sfx}").mkdir(parents=True, exist_ok=True)
            logging.warning(
                f"""A file with the same name as the given 'save_dir' already exists.
    A random suffix _{sfx} was added to the save directory."""
            )

    @retry(
        retry_on_exceptions=(
            requests.exceptions.ConnectionError,
            requests.exceptions.HTTPError,
        ),
        max_calls_total=4,
        retry_window_after_first_call_in_seconds=60,
    )
    def _get_data(
        self, url: str, chunk_size: int, queue: multiprocessing.Queue
    ) -> None:
        """Download data from the given URL in chunks and put them into a queue."""
        response = requests.get(url, stream=True)
        response.raise_for_status()  # raise an exception for bad requests

        for chunk in response.iter_content(chunk_size=chunk_size):
            if chunk:  # filter out keep-alive new chunks
                queue.put(chunk)

        # indicate that downloading is done
        queue.put(None)

    def _write_data(
        self, file_path: pathlib.Path, queue: multiprocessing.Queue
    ) -> None:
        """Write data chunks from the queue to a file."""
        with open(file_path, "wb") as fh:
            while True:
                chunk = queue.get()
                if chunk is None:
                    break
                fh.write(chunk)

    def _download_file(
        self, url: str, file_path: pathlib.Path, chunk_size: int = 1000
    ) -> None:
        """Coordinate the downloading and writing of a file using subprocesses."""
        queue = multiprocessing.Queue()

        # create and start subprocesses
        download_process = multiprocessing.Process(
            target=self._get_data, args=(url, chunk_size, queue)
        )
        download_process.start()
        write_process = multiprocessing.Process(
            target=self._write_data, args=(file_path, queue)
        )
        write_process.start()

        # wait for subprocesses to finish
        download_process.join()
        write_process.join()

    def download_data(
        self, the_date: str, data_type: str = "qbody", save_dir: str = SAVE_DIR
    ) -> None:
        """Download data for the given date and save it to the specified directory."""
        # parse 'the_date' into a datetime.date object
        the_date = dateutil.parser.parse(the_date).date()

        self._make_save_dir(save_dir)

        for url in self._generate_urls(the_date, data_type):
            # infer local filename
            qfile = pathlib.Path(save_dir, url.split("/")[-1])
            try:
                self._download_file(url, qfile, 1000)
                logging.info(f"Data downloaded and saved to {qfile}.")
            except:
                logging.error(f"Failed to download file {url}")


if __name__ == "__main__":
    downloader = CDDISDownloader()
    # the_date = datetime.date(2023, 1, 1)
    the_date = "2023 1 1"
    # the_date = datetime.date(2024, 3, 31)
    for t in DATA_TYPES.values():
        downloader.download_data(the_date, t)
