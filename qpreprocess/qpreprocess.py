# -*- coding: utf-8 -*-

"""qpreprocess.py


Preprocess quaternion files:

    - detect and remove outliers
    - detect non-normalized quaternions (discard or fix)
    - interpolate quaternions (using SLERP) and rotation angles
"""

import datetime
import dateutil
import logging
import pathlib

import numpy as np
import pandas as pd
import scipy

from date_utils import infer_dates, check_year


BASE_PATH = "./q"
SATELLITE = "ja3"  # ja1, ja2, ja3 or swo
DATA_TYPES = {"body": "qbody", "panels": "qsolp"}
LOGGING_LEVEL = logging.INFO
# LOGGING_LEVEL = logging.DEBUG  # try logging.DEBUG for more info

logging.basicConfig(
    format="%(module)s -- %(levelname)s: %(message)s", level=LOGGING_LEVEL
)


class Attitude:
    """Generic satellite attitude class."""

    def __init__(
        self, the_date: str, base_path: str = BASE_PATH, satellite: str = SATELLITE
    ) -> None:
        """Default constructor."""
        self.df = pd.DataFrame()
        self.the_date = dateutil.parser.parse(the_date).date()
        self.path = pathlib.Path(base_path)
        self.satellite = SATELLITE

    def _get_directoty_listing(
        self, path: pathlib.Path, pattern: str
    ) -> list[pathlib.Path]:
        """List the files in the local data directory."""
        # pattern = f'{self.satellite}*'
        return list(path.glob(pattern))

    def _generate_filenames(self, data_type: str) -> list[pathlib.Path]:
        """Generate the list of filenames for the given date and data type."""
        # check if the "extended" week ends in the next year
        the_dates = check_year(infer_dates(self.the_date))

        # for each sublist of 'the_dates' generate the filenames
        fnames = []
        for date_list in the_dates:
            # construct the filename pattern
            patterns = [
                f"{self.satellite}{data_type}{d.strftime('%Y%m%d')}*" for d in date_list
            ]
            logging.debug(patterns)

            for p in patterns:
                # append the existing filenames matching the pattern to the filenames list
                fnames.append(self._get_directoty_listing(self.path, p))
        logging.debug(fnames)

        # when done, flatten and return the filenames list
        return [fn for fns in fnames for fn in fns]

    def _read_file(self, path: pathlib.Path) -> pd.DataFrame:
        """Read a data file and return a DataFrame object."""
        df = pd.read_csv(path, sep="\s+", header=None, comment="#")

        # convert the first two columns to a datetime64 object
        df["date_time"] = pd.to_datetime(df[0]) + pd.to_timedelta(df[1])
        df.drop(columns=[0, 1], inplace=True)

        # set the new column as the DataFrame index
        df.set_index("date_time", inplace=True)

        return df

    def _read_data(self, data_type: str) -> None:
        """Merge the appropriate attitude files into a single DataFrame."""
        df_list = []
        for fn in self._generate_filenames(data_type):
            df_list.append(self._read_file(fn))

        self.df = pd.concat(df_list)


class Body(Attitude):
    """Class for satellite body attitude info."""

    def __init__(
        self, the_date: str, base_path: str = BASE_PATH, satellite: str = SATELLITE
    ) -> None:
        """Constructor."""
        self.data_type = 'qbody'

        # invoke the parent class constructor
        Attitude.__init__(self, the_date, base_path, satellite)

        # populate the DataFrame
        self._read_data(self.data_type)

        # drop unneeded columns; fix column names
        self.df.drop(columns=[2, 4, 5, 7, 8, 10, 11, 13], inplace=True)
        self.df.columns = ['q1', 'q2', 'q3', 'q4']


class Panels(Attitude):
    """Class for satellite solar panels' attitude info."""

    def __init__(
        self, the_date: str, base_path: str = BASE_PATH, satellite: str = SATELLITE
    ) -> None:
        """Constructor."""
        self.data_type = 'qsolp'

        # invoke the parent class constructor
        Attitude.__init__(self, the_date, base_path, satellite)

        # populate the DataFrame
        self._read_data(self.data_type)

        # drop unneeded columns; fix column names
        self.df.drop(columns=[2, 4, 5, 7], inplace=True)
        self.df.columns = ['left', 'right']


if __name__ == "__main__":
    the_date = "2023 1 1"

    body = Body(the_date)
    logging.info(body.df)

    panels = Panels(the_date)
    logging.info(panels.df)
