"""preprocess attitude -- download/interpolate quaternion files.

requires:
  - datetime
  - pathlib
  - re
  - shutil

  - requests
  - pandas
  - numpy
  - scipy
  - matplotlib
"""

