# -*- coding: utf-8 -*-

"""date_utils.py

Various date handling utilities.
"""

import datetime
import logging


LOGGING_LEVEL = logging.INFO
# LOGGING_LEVEL = logging.DEBUG  # try logging.DEBUG for more info

logging.basicConfig(
    format="%(module)s -- %(levelname)s: %(message)s", level=LOGGING_LEVEL
)


def infer_dates(the_date: datetime.date) -> list[datetime.date]:
    """Find the "extended" week that contains 'the_date'."""
    # day difference of start of the "extended" week, depending on the weekday of 'the_date'
    day_difference = {
        0: -3,
        1: -4,
        2: -5,
        3: -6,
        4: -7,
        5: -8,
        6: -2,
    }  # 0: Mon, ..., 6: Sun

    # begining of the "extended" week; Fri of the previous week
    begin_week = the_date + datetime.timedelta(days=day_difference[the_date.weekday()])

    # add the next 8 days; until Sat of the current week (data until Mon of the following week)
    the_dates = [begin_week + datetime.timedelta(days=d) for d in range(9)]

    # # return the starting dates in the format of the filenames
    # logging.debug([d.strftime("%Y%m%d") for d in the_dates])
    # return [d.strftime("%Y%m%d") for d in the_dates]
    logging.debug(the_dates)
    return the_dates


def check_year(the_dates: list[datetime.date]) -> list[list[datetime.date]]:
    """Split the date list in two if the "extended" week ends in a different year."""
    the_date_list = []
    the_date_list.append([d for d in the_dates if d.year == the_dates[0].year])

    # append the rest of the dates, if any
    if the_dates[0].year == the_dates[-1].year:
        logging.debug(the_date_list)
        return the_date_list

    the_date_list.append([d for d in the_dates if d.year == the_dates[-1].year])

    logging.debug(the_date_list)
    return the_date_list
