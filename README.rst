Attitude
--------

Compute satellite attitude; represented as **quaternions**.

Currently, supports measured attitude.

TODO: Add support for nominal attitude (AKA "*Phase Law*").
